<?php
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Pager
 */

/**
 * @ingroup Pager
 */
class GlobalBlockListPager extends TablePager {

	protected $conds;
	protected $page;

	/**
	 * @param SpecialPage $page
	 * @param array $conds
	 */
	function __construct($page, $conds) {
		$this->page = $page;
		$this->conds = $conds;
		$this->mDefaultDirection = IndexPager::DIR_DESCENDING;
		parent::__construct($page->getContext());

		$this->mDb = $this->getDatabase();
	}

	function getFieldNames() {
		static $headers = null;

		if ($headers === null) {
			$headers = [
				'timestamp' => 'blocklist-timestamp',
				'target' => 'blocklist-target',
				'expiry' => 'blocklist-expiry',
				'performer' => 'blocklist-by',
				'params' => 'blocklist-params',
				'reason' => 'blocklist-reason',
			];
			foreach ($headers as $key => $val) {
				$headers[$key] = $this->msg($val)->text();
			}
		}

		return $headers;
	}

	function formatValue($name, $value) {
		static $msg = null;
		if ($msg === null) {
			$keys = [
				'anononlyblock',
				'createaccountblock',
				'noautoblockblock',
				'emailblock',
				'blocklist-nousertalk',
				'unblocklink',
				'change-blocklink',
			];

			foreach ($keys as $key) {
				$msg[$key] = $this->msg($key)->escaped();
			}
		}

		/** @var $row object */
		$row = $this->mCurrentRow;
		$globalBlock = GlobalBlock::newFromRow($row);

		$language = $this->getLanguage();

		$formatted = '';

		switch ($name) {
			case 'timestamp':
				$formatted = htmlspecialchars($language->userTimeAndDate($value, $this->getUser()));
				break;

			case 'target':
				list($target, $type) = Block::parseTarget($globalBlock->getTarget());
				switch ($type) {
					case Block::TYPE_USER:
					case Block::TYPE_IP:
						$formatted = Linker::userLink($target->getId(), $target);
						$formatted .= Linker::userToolLinks(
							$target->getId(),
							$target,
							false,
							Linker::TOOL_LINKS_NOBLOCK
						);
						break;
					case Block::TYPE_RANGE:
						$formatted = htmlspecialchars($target);
				}
				break;

			case 'expiry':
				$formatted = htmlspecialchars($language->formatExpiry(
					$value,
					/* User preference timezone */true
				));
				if ($this->getUser()->isAllowed('block')) {
					$links[] = Linker::linkKnown(
						SpecialPage::getTitleFor('Unblock', $globalBlock->getTarget()),
						$msg['unblocklink']
					);
					$links[] = Linker::linkKnown(
						SpecialPage::getTitleFor('Block', $globalBlock->getTarget()),
						$msg['change-blocklink']
					);
					$formatted .= ' ' . Html::rawElement(
							'span',
							[ 'class' => 'mw-blocklist-actions' ],
							$this->msg('parentheses')->rawParams(
								$language->pipeList($links))->escaped()
						);
				}
				if ($value !== 'infinity') {
					$timestamp = new MWTimestamp($value);
					$formatted .= '<br />' . $this->msg(
						'ipb-blocklist-duration-left',
						$language->formatDuration(
							$timestamp->getTimestamp() - time(),
							// reasonable output
							[
								'minutes',
								'hours',
								'days',
								'years',
							]
						)
					)->escaped();
				}
				break;

			case 'performer':
				$performer = $globalBlock->getPerformer();
				if ($performer !== false) {
					$formatted = Linker::userLink($value, $performer->getName());
					$formatted .= Linker::userToolLinks($value, $performer->getName());
				} else {
					$formatted = htmlspecialchars($globalBlock->getPerformerName()); // foreign user?
				}
				break;

			case 'reason':
				$formatted = $globalBlock->getReason();
				break;

			case 'params':
				$properties = [];
				if ($globalBlock->prevents('blocks_create_account')) {
					$properties[] = $msg['createaccountblock'];
				}
				if ($row->global_id && !$row->enable_autoblock) {
					$properties[] = $msg['noautoblockblock'];
				}

				if ($globalBlock->prevents('blocks_email')) {
					$properties[] = $msg['emailblock'];
				}

				if ($globalBlock->prevents('blocks_edit_usertalk')) {
					$properties[] = $msg['blocklist-nousertalk'];
				}

				$formatted = $language->commaList($properties);
				break;

			default:
				$formatted = "Unable to format $name";
				break;
		}

		return $formatted;
	}

	function getQueryInfo() {
		$info = [
			'tables' => ['global_block'],
			'fields' => [
				'gbid',
				'target',
				'global_id',
				'performer_global_id',
				'performer_name',
				'reason',
				'timestamp',
				'enable_autoblock',
				'expiry',
				'ipb_range_start',
				'ipb_range_end',
				'deleted',
				'blocks_create_account',
				'blocks_email',
				'blocks_edit_usertalk',
				'site_key',
				'wiki_name'
			],
			'conds' => $this->conds
		];

		# Filter out any expired blocks
		$db = $this->getDatabase();
		$info['conds'][] = 'expiry > ' . $db->addQuotes($db->timestamp());

		# Is the user allowed to see hidden blocks?
		if (!$this->getUser()->isAllowed('hideuser')) {
			$info['conds']['deleted'] = 0;
		}

		return $info;
	}

	protected function getTableClass() {
		return parent::getTableClass() . ' mw-blocklist';
	}

	public function getIndexField() {
		return 'timestamp';
	}

	public function getDefaultSort() {
		return 'timestamp';
	}

	public function isFieldSortable($name) {
		return false;
	}

	/**
	 * Do a LinkBatch query to minimise database load when generating all these links
	 * @param ResultWrapper $result
	 */
	public function preprocessResults($result) {
		# Do a link batch query
		$lb = new LinkBatch;
		$lb->setCaller(__METHOD__);

		foreach ($result as $row) {
			$lb->add(NS_USER, $row->target);
			$lb->add(NS_USER_TALK, $row->target);

			if (isset($row->performer_name)) {
				$lb->add(NS_USER, $row->performer_name);
				$lb->add(NS_USER_TALK, $row->performer_name);
			}
		}

		$lb->execute();
	}

	/**
	 * Get the Database object in use.
	 *
	 * @return IDatabase
	 */
	public function getDatabase() {
		return GlobalBlock::getMasterDatabase();
	}
}
