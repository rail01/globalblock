<?php
/**
 * Implements Special:GlobalBlockList
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup SpecialPage
 */

/**
 * A special page that lists existing blocks
 *
 * @ingroup SpecialPage
 */
class SpecialGlobalBlockList extends SpecialPage {
	protected $target;

	protected $options;

	function __construct() {
		parent::__construct('GlobalBlockList');
	}

	/**
	 * Main execution point
	 *
	 * @param string $par Title fragment
	 */
	public function execute($par) {
		$this->setHeaders();
		$this->outputHeader();
		$out = $this->getOutput();
		$lang = $this->getLanguage();
		$out->setPageTitle($this->msg('globalblocklist'));
		$out->addModuleStyles([ 'mediawiki.special', 'mediawiki.special.blocklist' ]);

		$request = $this->getRequest();
		$par = $request->getVal('ip', $par);
		$this->target = trim($request->getVal('wpTarget', $par));

		$this->options = $request->getArray('wpOptions', []);

		$action = $request->getText('action');

		if ($action == 'unblock' || $action == 'submit' && $request->wasPosted()) {
			# B/C @since 1.18: Unblock interface is now at Special:Unblock
			$title = SpecialPage::getTitleFor('Unblock', $this->target);
			$out->redirect($title->getFullURL());

			return;
		}


		$db = GlobalBlock::getMasterDatabase(); //Just testing the connection before attempting to do anything else so that we can throw an error page.
		if ($db === false) {
			$this->getOutput()->showErrorPage('global_block_error', 'master_database_exception');
			return;
		}

		# setup GlobalBlockListPager here to get the actual default Limit
		$pager = $this->getGlobalBlockListPager();

		# Just show the block list
		$fields = [
			'Target' => [
				'type' => 'user',
				'label-message' => 'ipaddressorusername',
				'tabindex' => '1',
				'size' => '45',
				'default' => $this->target,
			],
			'Options' => [
				'type' => 'multiselect',
				'options-messages' => [
					'blocklist-userblocks' => 'userblocks',
					'blocklist-tempblocks' => 'tempblocks',
					'blocklist-addressblocks' => 'addressblocks',
					'blocklist-rangeblocks' => 'rangeblocks',
				],
				'flatlist' => true,
			],
			'Limit' => [
				'type' => 'limitselect',
				'label-message' => 'table_pager_limit_label',
				'options' => [
					$lang->formatNum(20) => 20,
					$lang->formatNum(50) => 50,
					$lang->formatNum(100) => 100,
					$lang->formatNum(250) => 250,
					$lang->formatNum(500) => 500,
				],
				'name' => 'limit',
				'default' => $pager->getLimit(),
			],
		];
		$context = new DerivativeContext($this->getContext());
		$context->setTitle($this->getPageTitle()); // Remove subpage
		$form = HTMLForm::factory('ooui', $fields, $context);
		$form->setMethod('get');
		$form->setWrapperLegendMsg('ipblocklist-legend');
		$form->setSubmitTextMsg('ipblocklist-submit');
		$form->setSubmitProgressive();
		$form->prepareForm();

		$form->displayForm(false);
		$this->showList($pager);
	}

	/**
	 * Setup a new GlobalBlockListPager instance.
	 * @return GlobalBlockListPager
	 */
	protected function getGlobalBlockListPager() {
		$conds = [];
		# Is the user allowed to see hidden blocks?
		if (!$this->getUser()->isAllowed('hideuser')) {
			$conds['deleted'] = 0;
		}

		if ($this->target !== '') {
			list($target, $type) = Block::parseTarget($this->target);

			switch ($type) {
				case Block::TYPE_ID:
				case Block::TYPE_AUTO:
					$conds['gbid'] = $target;
					break;

				case Block::TYPE_IP:
				case Block::TYPE_RANGE:
					list($start, $end) = IP::parseRange($target);
					$dbr = GlobalBlock::getMasterDatabase(DB_SLAVE);
					$conds[] = $dbr->makeList(
						[
							'target' => $target,
							Block::getRangeCond($start, $end)
						],
						LIST_OR
					);
					$conds['auto'] = 0;
					break;

				case Block::TYPE_USER:
					$conds['target'] = $target->getName();
					break;
			}
		}

		# Apply filters
		if (in_array('userblocks', $this->options)) {
			$conds['global_id'] = 0;
		}
		if (in_array('tempblocks', $this->options)) {
			$conds['expiry'] = 'infinity';
		}
		if (in_array('addressblocks', $this->options)) {
			$conds[] = "global_id != 0 OR ipb_range_end > ipb_range_start";
		}
		if (in_array('rangeblocks', $this->options)) {
			$conds[] = "ipb_range_end = ipb_range_start";
		}

		return new GlobalBlockListPager($this, $conds);
	}

	/**
	 * Show the list of blocked accounts matching the actual filter.
	 * @param GlobalBlockListPager $pager The GlobalBlockListPager instance for this page
	 */
	protected function showList(GlobalBlockListPager $pager) {
		$out = $this->getOutput();

		# Check for other blocks, i.e. global/tor blocks
		$otherBlockLink = [];
		Hooks::run('OtherBlockLogLink', [ &$otherBlockLink, $this->target ]);

		# Show additional header for the local block only when other blocks exists.
		# Not necessary in a standard installation without such extensions enabled
		if (count($otherBlockLink)) {
			$out->addHTML(
				Html::element('h2', [], $this->msg('ipblocklist-localblock')->text()) . "\n"
			);
		}

		if ($pager->getNumRows()) {
			$out->addParserOutputContent($pager->getFullOutput());
		} elseif ($this->target) {
			$out->addWikiMsg('ipblocklist-no-results');
		} else {
			$out->addWikiMsg('ipblocklist-empty');
		}

		if (count($otherBlockLink)) {
			$out->addHTML(
				Html::rawElement(
					'h2',
					[],
					$this->msg('ipblocklist-otherblocks', count($otherBlockLink))->parse()
				) . "\n"
			);
			$list = '';
			foreach ($otherBlockLink as $link) {
				$list .= Html::rawElement('li', [], $link) . "\n";
			}
			$out->addHTML(Html::rawElement(
				'ul',
				[ 'class' => 'mw-ipblocklist-otherblocks' ],
				$list
			) . "\n");
		}
	}

	protected function getGroupName() {
		return 'users';
	}
}
